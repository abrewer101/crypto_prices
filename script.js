document.getElementById('profitForm').addEventListener('submit', function(event) {
    event.preventDefault();

    const buyPrice = document.getElementById('buyPrice').value;
    const sellPrice = document.getElementById('sellPrice').value;
    const amount = document.getElementById('amount').value;

    const profit = (sellPrice - buyPrice) * amount;
    document.getElementById('result').textContent = `Your profit would be: $${profit.toFixed(2)}`;
});
